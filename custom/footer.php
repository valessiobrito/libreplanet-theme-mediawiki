<?php 

function LibrePlanetFooter( $skin, $context ) {

echo '<div class="container">
<div class="row">
  <div class="col-md-4">
    <div class="free-software-foundation">
       <p><img src="/w/skins/Tweeki/custom/fsf-logo.svg" alt="FSF" style="margin-bottom: 1em;" /></p>
      <p>The Free Software Foundation (FSF) is a nonprofit with a worldwide mission to promote computer user freedom.</p>
       <p>We defend the rights of all software users. (<a href="http://fsf.org">Read more</a>)</p>
    </div>
  </div>
  <div class="col-md-offset-1 col-md-4">
    <div class="campaigns">
    <h4>Campaigns</h4>
    <ul><li><a href="https://www.fsf.org/campaigns/priority-projects/">High Priority Free Software Projects</a></li>
    	<li><a href="https://www.fsf.org/campaigns/freejs/">Free JavaScript</a></li>
    	<li><a href="https://www.fsf.org/campaigns/campaigns/secure-boot-vs-restricted-boot/">Secure Boot vs Restricted Boot</a></li>
    	<li><a href="https://www.gnu.org/">GNU Operating System</a></li>
    	<li><a href="https://defectivebydesign.org/">Defective by Design</a></li>
    	<li><i><a href="https://www.fsf.org/campaigns">See all campaigns</a></i></li>
    </ul>
    </div><!-- .campaigns -->
  </div>
  <div class="col-md-3">
  <div class="get-involved"><h4>Get Involved</h4>
  <ul>
      <li><a class="footer-link" href="https://www.fsf.org/about/contact">Contact</a></li>
 </ul>
 </div><!-- .get-involved -->
</div>
</div>

<div class="container copyright">
	<p>Send your feedback on our translations and new translations of pages to <a href="mailto:campaigns@fsf.org">campaigns@fsf.org</a>.</p>

	<p>Copyright © 2013–2019 <a href="//fsf.org/about/contact.html" title="Find out how to contact us">Free Software Foundation</a>, Inc.
	<a href="https://www.fsf.org/about/free-software-foundation-privacy-policy">Privacy Policy</a>, <a href="/weblabels" rel="jslicense">JavaScript license information</a>
        </p>
</div>

';

}

?>
